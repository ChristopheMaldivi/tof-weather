import {Forecast} from "@/domain/Forecast";

export class DayForecasts {
    constructor(
        readonly day: string,
        readonly forecasts: Array<Forecast>
    ) {
    }
}