import {GeoPosition} from "@/infrastructure/geolocation/GeoPosition";
import {Weather} from "@/domain/Weather";
import {DayForecasts} from "@/domain/DayForecasts";

export class Location {

    static refresh(
        refreshLocation: (geoPosition: GeoPosition) => void,
        refreshLocationError: (error: string) => void,
        refreshWeather: (weather: Array<DayForecasts>) => void) {
        const callback = async (geoPosition: GeoPosition | undefined, error: string | undefined) => {
            if (geoPosition) {
                const result = await geoPosition.retrieveCityName();
                if (result instanceof GeoPosition) {
                    geoPosition = result;

                    // FIXME, refacto
                    await Weather.refresh(geoPosition, refreshWeather);

                } else {
                    refreshLocationError(result);
                }
                refreshLocation(geoPosition);
            } else if (error) {
                refreshLocationError(error);
            }
        }

        GeoPosition.retrieveGeoLocation(callback);
    }
}