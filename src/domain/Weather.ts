import {WeatherClient} from "@/infrastructure/weather/WeatherClient";
import {GeoPosition} from "@/infrastructure/geolocation/GeoPosition";
import {DayForecasts} from "@/domain/DayForecasts";

export class Weather {

    static async refresh(
        geoPosition: GeoPosition, refreshWeather: (weather: Array<DayForecasts>) => void
    ) {
        const weather = await WeatherClient.retrieveWeatherInfo(geoPosition)
        refreshWeather(weather);
    }
}