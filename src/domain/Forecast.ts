export class Forecast {
    constructor(
        readonly day: string,
        readonly hour: number,
        readonly weatherType: string | undefined,
        readonly temperature: number,
        readonly probaRain: number) {
    }
}