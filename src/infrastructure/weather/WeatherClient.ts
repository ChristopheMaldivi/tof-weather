import axios from 'axios';
import {GeoPosition} from "@/infrastructure/geolocation/GeoPosition";
import {TokenPersistence} from "@/infrastructure/config/TokenPersistence";
import {WeatherType} from "@/infrastructure/weather/WeatherType";
import {Forecast} from "@/domain/Forecast";
import {DayForecasts} from "@/domain/DayForecasts";

export class WeatherClient {

    static async retrieveWeatherInfo(geoLocation: GeoPosition): Promise<Array<DayForecasts>> {
        const url = "https://api.meteo-concept.com/api/forecast/daily/periods?" +
            "token=" + TokenPersistence.getToken() +
            "&latlng=" + geoLocation.latitude + "," + geoLocation.longitude;
        try {
            const response = await axios.get(url);
            return WeatherClient.extractData(response.data.forecast);
        } catch (error) {
            return error.message;
        }
    }

    private static extractData(forecasts: Array<Array<any>>): Array<DayForecasts> {
        return forecasts.map(rawForecasts => {
            const forecasts: Array<Forecast> = rawForecasts
                .filter(forecast => {
                    const date = new Date(forecast.datetime);
                    return new Date() < date;
                })
                .map(forecast => {
                    const date = new Date(forecast.datetime);
                    const dayName = date.toLocaleDateString("fr-fr", {weekday: 'long'});
                    const weatherType = WeatherType.of(forecast.weather);
                    return new Forecast(
                        dayName,
                        date.getHours(),
                        weatherType,
                        forecast.temp2m,
                        forecast.probarain);
                });

            if (forecasts.length == 0) {
                return new DayForecasts("-", forecasts);
            }
            return new DayForecasts(forecasts[0].day, forecasts);
        });
    }

}
