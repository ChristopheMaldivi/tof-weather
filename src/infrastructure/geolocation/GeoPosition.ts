import axios from 'axios';

export class GeoPosition {
    public readonly latitude: number;
    public readonly longitude: number;
    public readonly cityName: string | undefined;
    public readonly cityCode: string | undefined;

    constructor(latitude: number, longitude: number, cityName: string | undefined = undefined, cityCode: string | undefined = undefined) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.cityName = cityName;
        this.cityCode = cityCode;
    }

    withCity(cityName: string, cityCode: string): GeoPosition {
        return new GeoPosition(this.latitude, this.longitude, cityName, cityCode);
    }

    async retrieveCityName(): Promise<GeoPosition | string> {
        const url = "https://nominatim.openstreetmap.org/reverse?format=json&zoom=18&addressdetails=1" +
            "&lat=" + this.latitude +
            "&lon=" + this.longitude;
        try {
            const response = await axios.get(url);
            return this.withCity(response.data.address.town, response.data.address.postcode.toString());
        } catch (error) {
            return error.message;
        }
    }

    public static retrieveGeoLocation(callback: (geoPosition: GeoPosition | undefined, error: string | undefined) => void) {
        navigator.geolocation.getCurrentPosition(position => {
                const latitude = parseFloat(position.coords.latitude.toFixed(3));
                const longitude = parseFloat(position.coords.longitude.toFixed(3));
                callback(new GeoPosition(latitude, longitude), undefined);
            },
            error => {
                callback(undefined, error.message);
            });
    }
}
