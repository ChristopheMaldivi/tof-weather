export class TokenPersistence {
    private static readonly TOKEN_KEY: string = "token-key";
    private static token: string | undefined;

    static updateToken(newToken: string) {
        TokenPersistence.token = newToken;
        localStorage.setItem(TokenPersistence.TOKEN_KEY, newToken)
    }

    static getToken() {
        const optionalToken = localStorage.getItem(TokenPersistence.TOKEN_KEY)
        return optionalToken == null ? undefined : optionalToken;
    }
}
